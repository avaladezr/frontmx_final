console.log("Aquí funcionando con nodemon")

var movimientoJSON = require('./movimientosv2.json')
var express = require('express')
var app = express()

app.get('/', function(req, res)
{
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req,res)
{
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req,res)
{
  res.sendfile('movimientosv2.json')
})

app.get('/v2/movimientos/:id', function(req,res)
{
  //console.log(req)
  console.log(req.params.id)
  //console.log (movimientoJSON)
  //res.send('Hemos recibido su petición de consulta del movimiento #' + req.params.id)
  res.send(movimientoJSON[req.params.id-1])
})

app.listen(3000)

console.log("Escuchando en el puerto 3000")
